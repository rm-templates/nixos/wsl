# THIS IS JUST A TEMPLATE
# DO NOT LINK
# TO USE COPY TO _.nix AND EDIT THERE
{
  proxy = {
    ip = "169.254.254.1";
    port = "3128";
    no_proxy = "127.0.0.1,wsl.host,localhost,localhost,169.254.254.1"
  };
  git = {
    user = "<git user>";
    email = "<git email>";
  };
}