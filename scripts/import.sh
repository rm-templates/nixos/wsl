#!/bin/sh

BASEDIR=$(dirname $0)

echo "importing from /etc/nixos"

rm -rf $BASEDIR/../nixos/*
cp -R /etc/nixos/* $BASEDIR/../nixos/

if [ ! -d $BASEDIR/../credentials ]; then
 mkdir $BASEDIR/../credentials
fi

mv $BASEDIR/../nixos/credentials/* $BASEDIR/../credentials
rm -rf $BASEDIR/../nixos/credentials