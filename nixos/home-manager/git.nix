{ config, pkgs, lib, ... }:
let
  creds = (import ../credentials/_.nix).git;
  userName = creds.user;
  userMail = creds.email;
in
{
  home-manager.users.${config.mainUser} = {
    programs.git = {
      enable = true;
      userEmail = "${userMail}";
      userName = "${userName}";

      extraConfig = {
        init = {
          defaultBranch = "main";
        };
      };
    };
  };
}
