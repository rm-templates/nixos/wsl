{ config, pkgs, ... }:
{  

  imports = [
    ./powerline-go.nix
    ./bash.nix
    ./starship.nix
    ./kitty.nix
  ];

  environment.systemPackages = with pkgs; [
    kitty
  ];

}
