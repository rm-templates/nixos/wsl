{ config, pkgs, ...}:
{

  users.users.${config.mainUser} = {
    packages = with pkgs; [
      direnv
    ];
  };

  environment.interactiveShellInit = ''
    eval "$(direnv hook bash)"
  '';
}