{ config, pkgs, ...}:
{
   home-manager.users.${config.mainUser} = {
    programs.bash = {
      enable = true;

      initExtra = ''
         alias la="ls -la --color=auto"

        source <(kubectl completion bash)
        alias k=kubectl
        alias kns="kubectl config set-context --current --namespace "
        complete -F __start_kubectl k
      '';
      
    };
  };
}
