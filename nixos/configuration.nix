# Edit this configuration file to define what should be installed on
# your system. Help is available in the configuration.nix(5) man page, on
# https://search.nixos.org/options and in the NixOS manual (`nixos-help`).

# NixOS-WSL specific options are documented on the NixOS-WSL repository:
# https://github.com/nix-community/NixOS-WSL

{ config, lib, pkgs, ... }:
let
  proxy = (import ./credentials/_.nix).proxy;
in
{
  imports = [
    # include NixOS-WSL modules
    <nixos-wsl/modules>

    ./user/_.nix
    ./appearance/_.nix
    ./home-manager/_.nix
    ./shell/_.nix

    (fetchTarball "https://github.com/nix-community/nixos-vscode-server/tarball/master")
  ];

  wsl.enable = true;
  wsl.defaultUser = "${config.mainUser}";
  

  # This value determines the NixOS release from which the default
  # settings for stateful data, like file locations and database versions
  # on your system were taken. It's perfectly fine and recommended to leave
  # this value at the release version of the first install of this system.
  # Before changing this value read the documentation for this option
  # (e.g. man configuration.nix or on https://nixos.org/nixos/options.html).
  system.stateVersion = "23.05"; # Did you read the comment?


  # ADDITIONS
  networking.proxy = {
    default = "http://${proxy.ip}:${proxy.port}";
    noProxy = ${proxy.no_proxy};
  };

  programs.nix-ld.enable = true;

  environment.systemPackages = with pkgs; [
    wget
    nmap
    inetutils
    yq-go
  ];

  users.users.${config.mainUser} = {
    isNormalUser = true;
    description = "${config.mainUser}";
    extraGroups = [ "wheel" ];
  };

  nixpkgs.config.allowUnfree = true;

  services.vscode-server.enable = true;

  wsl.extraBin = with pkgs; [
    { src = "${coreutils}/bin/uname"; }
    { src = "${coreutils}/bin/dirname"; }
    { src = "${coreutils}/bin/readlink"; }
    { src = "${coreutils}/bin/cut"; }
    { src = "${coreutils}/bin/nix"; }
    { src = "${coreutils}/bin/rm"; }
  ];

  nix.settings.experimental-features = [ "nix-command" "flakes" ];
}
