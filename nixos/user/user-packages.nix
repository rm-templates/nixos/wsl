{ config, pkgs, ...}:
{
  users.users.${config.mainUser} = {

    packages = with pkgs; [
      kate
      kubernetes-helm
      kubectl
      nodejs_20
      argocd
      openssl
    ];
  };


}
