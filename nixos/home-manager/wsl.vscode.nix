{ config, pkgs, ...}:
{
    home-manager.users.${config.mainUser} = {
        home.file = {
            ".vscode-server/server-env-setup" = {
                text = ''
                    export NIX_LD_LIBRARY_PATH=/run/current-system/sw/share/nix-ld/lib
                    export NIX_LD=/run/current-system/sw/share/nix-ld/lib/ld.so
                '';
            };
        };
    };

}