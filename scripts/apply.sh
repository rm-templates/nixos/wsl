#!/bin/sh

BASEDIR=$(dirname $0)


if [[ $EUID -ne 0 ]]; then
   echo "This script must be run as root" 
   exit 1
fi

echo "This will overwrite existing files in /etc/nixos. Are you sure you want to proceeed [y/n]"
read REPLY

if [[ "$REPLY" == "y" ]]; then
  echo "applying configuration to /etc/nixos ..."
  cp -r $BASEDIR/../nixos/* /etc/nixos
  cp $BASEDIR/../credentials/* /etc/nixos/credentials
  echo "done"
fi
