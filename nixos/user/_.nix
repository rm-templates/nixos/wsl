{ lib, config, ... }:
with lib;
with types;
{

  imports = [
    ./direnv.nix
    ./user-packages.nix
  ];

  options = {
    mainUser = mkOption {
      type = str;
      description = "main user for the operating system";
      default = "nixos";    
    };
  };
}