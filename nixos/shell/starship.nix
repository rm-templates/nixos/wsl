{ pkgs, config, ...}:
{

  # requires .programs.bash to be enabled in home-manager

  home-manager.users.${config.mainUser} = {

    programs.starship.enable = true;
    
    programs.starship.settings = {
      format = "[](#9A348E)$os$username[](bg:#DA627D fg:#9A348E)$directory[](fg:#DA627D bg:#FCA17D)$git_branch$git_status[](fg:#FCA17D bg:#86BBD8)$golang$package$rust[](fg:#86BBD8 bg:#33658A)$kubernetes$fill
[ ](bg:#33658A)$character(bg:none)";
      

      os = {
        tyle = "bg:#9A348E";
        disabled = true; # Disabled by default
      };

      username = {
        disabled = true;
        show_always = true;
        style_user = "bg:#9A348E";
        style_root = "bg:#9A348E";
        format = "[$user ]($style)";
      };

      directory = {
        style = "bg:#DA627D";
        format = "[ $path ]($style)";
        truncation_length = 3;
        truncation_symbol = "…/";
        substitutions = {
          "Documents" = "󰈙 ";
          "Downloads" = " ";
          "Music" = " ";
          "Pictures" = " ";
        };
      };

      git_branch = {
        symbol = "";
        style = "bg:#FCA17D";
        format = "[ $symbol $branch ]($style)";
      };

      git_status = {
        style = "bg:#FCA17D";
        format = "[$all_status$ahead_behind ]($style)";
        ahead = "⇡\${count} ";
        behind = "⇣\${count} ";
        modified = "\${count}✎ ";
        untracked = "\${count}+ ";
      };

      golang = {
        symbol = " ";
        style = "bg:#86BBD8";
        format = "[ $symbol ($version) ]($style)";
      };

      package = {
        disabled = false;
        symbol = "";
        style = "bg:#86BBD8";
        format = "[ $symbol ($version) ]($style)";
      };

      rust = {
        symbol = "";
        style = "bg:#86BBD8";
        format = "[ $symbol ($version) ]($style)";
      };

      kubernetes = {
        disabled = false;
        symbol = " ☸ ";
        format = "[$symbol$context >( \($namespace\))]($style)";
        style = "bg:#33658A";
      };

      fill = {
        style = "bg:#33658A";
        symbol = " ";
      };


      time = {
        disabled = true;
        time_format = "%R";
        style = "bg:#33658A";
        format = "[ ♥ $time ]($style)";
      };

      character = {
        success_symbol = "[ ](bg:#55eb34)[](fg:#55eb34 bg:none)";
        error_symbol = "[ ](bg:#d60427)[](fg:#d60427 bg:none)";
      };
    };
  };
}
