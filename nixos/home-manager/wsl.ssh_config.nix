{ config, pkgs, ...}:
let
    proxy = (import ../credentials/_.nix).proxy;
in
{
    home-manager.users.${config.mainUser} = {
        home.file = {
            ".ssh/config" = {
                text = ''
                    Host gitlab.com
                        ProxyCommand          nc -X connect -x ${proxy.ip}:${proxy.port} %h %p
                        ServerAliveInterval   10
                        IdentityFile          ~/.ssh/id_rsa

                    Host github.com
                        ProxyCommand          nc -X connect -x ${proxy.ip}:${proxy.port} %h %p
                        ServerAliveInterval   10
                        IdentityFile          ~/.ssh/id_rsa
                '';
            };
        };
    };

}