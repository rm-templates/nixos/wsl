{ pkgs, config, ...}:
{

  # requires .programs.bash to be enabled in home-manager

  home-manager.users.${config.mainUser} = {
    
    programs.powerline-go = {
      enable = true;
      modules = [
        "cwd"
        "git"
        "node"
        "docker"
        "kube"
        "root"
      ];

      newline = true;

      settings = {
        cwd-mode = "dironly";
      };
    };
  };
}
